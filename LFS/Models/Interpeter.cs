﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LFS.Models
{

    public class Interpeter
    {
        public static Token Solver(List<Token> eq)
        { 

            int rightpar_Ind = -1;
            int leftpar_Ind = -1;

        
           
            //Get Pathernthesis Index
            for (int i = 0; i < eq.Count; i++)
            {
                if (eq[i].Type == Token.TokenType.RightPar)
                {
                    rightpar_Ind = i;
                }
                if (eq[i].Type == Token.TokenType.LeftPar)
                {
                    leftpar_Ind = i;
                    var tempToken = ResolveEquation(eq.GetRange((rightpar_Ind + 1), (leftpar_Ind - rightpar_Ind - 1)));
                    eq.RemoveRange(rightpar_Ind, (leftpar_Ind - rightpar_Ind + 1));
                    eq.Insert(rightpar_Ind, tempToken);
                    i = -1;
                }

            }
            return eq[0];
         

        }
        private static Token ResolveEquation(List<Token> eq)
        {
            //Negation
            for (int i = 0; i < eq.Count; i++)
            {
                if (eq[i].Type == Token.TokenType.Negation)
                {
                    eq[i + 1] = Negation(eq[i + 1].Value);
                    eq.RemoveAt(i);
                    i = 0;
                }
               

            }
            //And
            for (int i = 0; i < eq.Count; i++)
            {
                if (eq[i].Type == Token.TokenType.And)
                {

                    eq[i - 1] = XandY(eq[i + 1].Value, eq[i - 1].Value);
                    eq.RemoveRange(i, 2);
                    i = 0;
                }
            }

            //OR
            for (int i = 0; i < eq.Count; i++)
            {
                if (eq[i].Type == Token.TokenType.Or)
                {

                    eq[i - 1] = XorY(eq[i + 1].Value, eq[i - 1].Value);
                    eq.RemoveRange(i, 2);
                    i = 0;
                }
            }
            //Bi Condition 
            for (int i = 0; i < eq.Count; i++)
            {
                if (eq[i].Type == Token.TokenType.biCondition)
                {

                    eq[i - 1] = BiConditional(eq[i + 1].Value, eq[i - 1].Value);
                    eq.RemoveRange(i, 2);
                    i = 0;
                }
            }
            //Implies
            for (int i = 0; i < eq.Count; i++)
            {
                if (eq[i].Type == Token.TokenType.Implies)
                {

                    eq[i - 1] = MaterialImplication(eq[i - 1].Value, eq[i + 1].Value);
                    eq.RemoveRange(i, 2);
                    i = 0;
                }
            }
            if (eq.Count == 1)
               return eq[0];
            else
                throw new WrongFormulaException();

        }
        public static Token Negation (bool x)
        {
            return new Token(passChar(!x));
        }


        public static Token XorY(bool x, bool y)
        {
            return new Token(passChar(x || y));
        }

        public static Token XandY(bool x, bool y)
        {
            return new Token(passChar(x && y));
        }

        public static Token Exclusive(bool x, bool y)
        {
            return new Token(passChar((x && y) && !(x && y)));// always false
        }

        public static Token  Tautoloty(bool x, bool y)
        {
            return new Token(passChar(x || y || (!x && !y)));// always true
        }

        private static Token Contradition(bool x, bool y)
        {
            return new Token(passChar( x || (y && !y)));
        }

        private static bool Implies(bool x, bool y)
        {
            return !x || y;
            //this can also be expressed as the following conjunction !(x && !y);
        }
        private static Token MaterialImplication(bool x, bool y)
        {
            return new Token(passChar(!x || y));


            //this can also be expressed as the following conjunction !(x && !y);
        }

        public static Token BiConditional(bool x, bool y)
        {
            return new Token(passChar(Implies(x, y) && Implies(y, x))); //"if and only if"
        }
        private static char passChar (bool x)
        {
            if (x)
                return '1';
            else
                return '0';
        }
    }


}
