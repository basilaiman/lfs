﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Solve.aspx.cs" Inherits="LFS.Solve" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phBody" runat="server">
    <section id="solve">
        <div class="container">
            <h2>Solve the equation </h2>
            <p class="lead">Write your Equation here and using the beneath operators 
            </p>
            <div id="operatorsDiv" class="text-center">
                <span onclick="writeOperator(this);"> ¬ </span>
                 <span onclick="writeOperator(this);"> ^ </span>
                 <span onclick="writeOperator(this);"> ∨ </span>
                 <span onclick="writeOperator(this);"> → </span> 
                <span onclick="writeOperator(this);"> ↔ </span>
            </div>

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control input-lg" id="txtEquation" runat="server" placeholder="Put your Equation .. Ex: X ∧ Y" />
                        
                        <span class="input-group-btn">
                            <button class="btn btn-danger btn-lg" type="button" runat="server" id="btnSolve" onserverclick="btnSolve_ServerClick">Solve it</button>
                        </span>
                    
                    </div>
                    <asp:Label id="lblException" runat="server" />
                    <asp:Label Text="" ID="lblResult" runat="server" />
                    <table class="table table-striped"  id="tblResult" runat="server">
                        <thead>
                            <tr runat="server" id="tblHead">
                               
                                
                            </tr>
                        </thead>
                        <tbody>
                         
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <script>
        function writeOperator (el){
            var op = $(el).text();
            var val = $("#phBody_txtEquation").val();
            $("#phBody_txtEquation").val(val + op);
        }
        
    </script>
</asp:Content>
