﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LFS.Models
{
    public class Token
    {
        public Token(char Op)
        {
            //Tokenize the Token 
            if (Op == '0')
            {
                this.Value = false;
                this.Type = TokenType.Value;
            }
            else if (Op == '1')
            {
                this.Value = true;
                this.Type = TokenType.Value;
            }
            else if (Char.IsLetter(Op))
            {
                this.Type = TokenType.Char;
            }
            else if (Op == '(')
                this.Type = TokenType.RightPar;
            else if (Op == ')')
                this.Type = TokenType.LeftPar;
            else if (Op == '^')
                this.Type = TokenType.And;
            else if (Op == '∨')
                this.Type = TokenType.Or;
            else if (Op == '¬')
                this.Type = TokenType.Negation;
            else if (Op == '→')
                this.Type = TokenType.Implies;
            else if (Op == '↔')
                this.Type = TokenType.biCondition;

            this.op = Op;
        }
        //Put the Token Type
        public enum TokenType
        {
            None,
            Value,
            Char,
            LeftPar,
            RightPar,
            And,
            Or,
            Negation,
            Implies,
            biCondition
        };
        public bool Value { get; set; } // The Value
        public char op { get; set; } //The string represntive of the Token
        public TokenType Type { get; set; }
        //Split the Equation to Tokens
        public static List<Token> Tokenize(string input)
        {
            string lowerInput = input.ToLower();
            string equation = Regex.Replace(lowerInput, @"\s+", "");


            foreach (char item in equation)
            {
                if (item != ' ')
                {
                    equation = equation.Replace(" ", "");
                }
            }
            List<Token> ouput = new List<Token>();
            foreach (char item in equation)
            {
                ouput.Add(new Token(item));

            }
            return ouput;
        }
        public static bool isParenthesisCorrect(List<Token> Equation)
        {
            //Check if parenthesis are correct
            Stack<Token> stack = new Stack<Token>();
            foreach (var item in Equation)
            {
                if (item.Type == TokenType.RightPar)
                {
                    stack.Push(item);
                }
                else if (item.Type == TokenType.LeftPar)
                {
                    if (stack.Count == 0)
                        throw new WrongParenthesisException();
                    stack.Pop();
                }
            }
            if (stack.Count == 0)
                return true;
            else
                return false;
        }
        public static bool Test(string equation)
        {
            var x = Tokenize(equation);
            var res = isParenthesisCorrect(x);
            return res;

        }


    }
}
