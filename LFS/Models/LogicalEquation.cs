﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFS.Models
{
    class LogicalEquation
    {
        public char[] Variables { get; set; }
        public List<Token> Equation { get; set; }
        public string sEquation { get; set; }
        public LogicalEquation(string eq)
        {
            sEquation = eq;
            Equation = Token.Tokenize(eq);
            if (!Token.isParenthesisCorrect(Equation))
                throw new WrongParenthesisException();
            //get the charchters in List
            List<char> temp = new List<char>();
            foreach (var item in Equation)
            {
                if (item.Type == Token.TokenType.Char)
                    temp.Add(item.op);
            }

            char [] output = temp.ToArray();
            string convert = null;
            for (int i = 0; i < output.Length; i++)
            {
                for (int j = i + 1; j < output.Length; j++)
                {
                    if (output[i] == output[j])
                    {
                        output[j] = ',';

                    }
                }

            }


            for (int i = 0; i < output.Length; i++)
            {
                if (output[i] == ',')
                {
                    continue;
                }
                convert = convert + output[i];
            }

             Variables =  convert.ToArray();

        }
    }
}
