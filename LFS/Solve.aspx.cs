﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LFS.Models;
using System.Web.UI.HtmlControls;

namespace LFS
{
    public partial class Solve : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Request.IsAuthenticated)
            {
                Response.Redirect("Default.aspx");

            }
            lblException.Text = "";
            lblException.Attributes["style"] = "color:red; font-weight:bold;";

        }

        protected void btnSolve_ServerClick(object sender, EventArgs e)
        {
            try
            {

            
          
                string input = txtEquation.Value;

                string eq = "(" + txtEquation.Value.ToString().ToLower() + ")";
                LogicalEquation myEq = new LogicalEquation(eq);
                LogicManager manager = new LogicManager(myEq);


                foreach (var item in myEq.Variables)
                {
                    tblHead.Cells.Add(new HtmlTableCell("td") { InnerText = item.ToString() });
                }
                tblHead.Cells.Add(new HtmlTableCell("td") { InnerText = eq });
                for (int i = 0; i < manager.numberOfCases; i++)
                {
                    HtmlTableRow tempRow = new HtmlTableRow();
                    foreach (var item in manager.TruthTableArray[i])
                    {
                        tempRow.Cells.Add(new HtmlTableCell() { InnerText = item.ToString() });
                    }
                    tempRow.Cells.Add(new HtmlTableCell() { InnerText = manager.TableResults[i].Value.ToString() });
                    tblResult.Rows.Add(tempRow);
                }



            }
            catch (Exception r)
            {

                lblException.Text = "Equation is not correct , Please fix the Equation and try again";
            }








        }
    }
}