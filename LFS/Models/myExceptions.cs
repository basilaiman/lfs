﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFS.Models
{
    class WrongParenthesisException : Exception
    {
        public WrongParenthesisException()
        {
           

        }

        public WrongParenthesisException(string message) : base(message)
        {

        }

        public WrongParenthesisException(string message, Exception inner) : base(message, inner)
        {

        }

    }
    class WrongFormulaException : Exception
    {
        public WrongFormulaException()
        {
        }

        public WrongFormulaException(string message) : base(message)
        {
        }

        public WrongFormulaException(string message, Exception inner) : base(message, inner)
        {

        }

    }
}
