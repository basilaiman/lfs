﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LFS

{

    
    public partial class Main : System.Web.UI.MasterPage
    {

        


        protected void Page_Load(object sender, EventArgs e)
        {
            // TODO : Validattor Password
            if (Request.IsAuthenticated)
            {
                divLogin.Visible = false;
                divLogout.Visible = true;
            }
           

        }

        
        public static bool LoginMethod(string username, string password, bool keepLoggedIn)
        {
            FormsAuthentication.SetAuthCookie(username, keepLoggedIn);
            if (keepLoggedIn)
            {
                FormsAuthenticationTicket ticket1 =
              new FormsAuthenticationTicket(
                   1,                                   // version
                  username,   // get username  from the form
                   DateTime.Now,                        // issue time is now
                   DateTime.Now.AddDays(7),         // expires in 10 minutes
                   true, null                            // role assignment is stored
                                                         // in userData
                   );
                HttpCookie cookie1 = new HttpCookie(
                  FormsAuthentication.FormsCookieName,
                  FormsAuthentication.Encrypt(ticket1));
            }
            return true;
        }





        

        protected void btnLoginSubmit_Click(object sender, EventArgs e)
        {
            if (Membership.ValidateUser(loginUsername.Value.Trim(), loginPassword.Value))
            {
                LoginMethod(loginUsername.Value.Trim(), loginPassword.Value, chkRemember.Checked);
                Response.Redirect("default.aspx");
            }

        }

        
        protected void btnRegSubmit_Click(object sender, EventArgs e)
        {
            MembershipUser newUser = Membership.CreateUser(regUsername.Value, regPassword.Value, regEmail.Value);
            Roles.AddUserToRole(regUsername.Value, "User");
            if (Membership.ValidateUser(regUsername.Value, regPassword.Value))
            {
                LoginMethod(regUsername.Value, regPassword.Value,false);
                Response.Redirect("default.aspx");
            }

        }

        protected void btnLogout_ServerClick(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("default.aspx");
        }
    }
}