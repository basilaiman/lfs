﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LFS.Models
{
    class LogicManager
    {
        public int numberOfVariables { set; get; }
        public int numberOfCases { set; get; }
        public int numberOfValues { set; get; }
        public char[][] TruthTableArray { set; get; }
        LogicalEquation myEquation;
        public Token[] TableResults { set; get; }
        public LogicManager(LogicalEquation num)
        {
            this.myEquation = num;
            numberOfVariables = num.Variables.Length;
            numberOfCases = Convert.ToInt32(Math.Pow(2, numberOfVariables));
            numberOfValues = 3 * numberOfCases;
            TruthTableArray = new char[numberOfCases][];
            for (int i = 0; i < numberOfCases; i++)
            {
                TruthTableArray[i] = new char[numberOfVariables];
            }
            generateTruthTable();
            SolveEquation();

        }

        private void generateTruthTable()
        {

            string coloumRes = String.Empty;
            for (int i = numberOfVariables - 1; i >= 0; i--)
            {
                int checkerNum = Convert.ToInt32(Math.Pow(2, i));
                int converter = 0;
                int counter = 0;
                bool toPut = false;
                while (counter < numberOfCases)
                {
                    if (converter == checkerNum)
                    {
                        toPut = !toPut;
                        converter = 0;
                    }

                    if (toPut)
                    {
                        coloumRes = coloumRes + "0";
                    }
                    else
                    {
                        coloumRes = coloumRes + "1";
                    }
                    counter++;
                    converter++;

                }
            }
            /*
            This above Code  is used to generate all the values of the truth table in a linear array 
            Ex : 
            A|B
            0|0
            0|1
            1|0
            1|1
            the Result will be all the first coloumn then the next one
            0011 0101
            */
            //Now let's convert it to Rows Result 
            //Man I'm a fucking genius !
            string result = string.Empty;
            for (int i = 0; i < numberOfCases; i++)
            {

                for (int j = 0; j < numberOfVariables; j++)
                {
                    result = result + coloumRes[i + (j * numberOfCases)];

                }
            }

            int arrCounter = 0;
            foreach (var item in TruthTableArray)
            {
                for (int i = 0; i < item.Length; i++)
                {
                    item[i] = result[arrCounter];
                    arrCounter++;

                }
            }
        }
        private void SolveEquation()
        {
            List<Token> Result = new List<Token>();
            foreach (var item in TruthTableArray)
            {
                // First Replace the variables in the Equation from the TruthTable 
                // Then Pass it to the Solver then add it the Result List
                Result.Add(Interpeter.Solver(ReplaceVarWithValues(item)));
            }
            TableResults = Result.ToArray();



        }
        List<Token> ReplaceVarWithValues(char[] values)
        {
            string eq = myEquation.sEquation;
            string equa = String.Empty;
            for (int i = 0; i < myEquation.Variables.Length; i++)
            {
                if (i == 0)
                    equa = eq.Replace(myEquation.Variables[i], values[i]);
                else
                    equa = equa.Replace(myEquation.Variables[i], values[i]); ;


            }
            return Token.Tokenize(equa);
        }

    }
}
