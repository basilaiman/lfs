﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="LFS.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="phHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="phBody" runat="server">
    <div class="cover" id="cover">
        <div class="cover-text">
            <h1>We will make your life EASIER</h1>
            <p class="lead">
                Welcome to Logic Formulas Interpterer! This system is capable of validating whether or not a given string of text is a Well Formed Formula or not, and give a person a visualization of that formula in the form of a Truth Table, and more !
            </p>
            <ul>
                <li id="btnLog" runat="server"><a href="#" data-toggle="modal" data-target="#loginModal" class="btn btn-danger btn-lg" role="button">Let's get started</a></li>
                <li id="btnSolve" visible="false" runat="server"><a href="Solve.aspx" class="btn btn-danger btn-lg" role="button">Let's get started</a></li>
            </ul>
            

        </div>
    </div>
    <section class="testimonials">
        <div class="container">
            <blockquote>
                &ldquo; Beauty is truth, truth beauty, that is all
     You know on Earth, and all you need to know. &rdquo;
            <cite>&mdash; John Keats, Ode On A Grecian Urn, 1819 </cite>
            </blockquote>
        </div>

    </section>
    <section id="why-us">
        <h2>Why LFI ?</h2>
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <h3>Simple</h3>
                    <p>
                        LFI is very user friendly and simple to use by anyone and familiar with logical students and researchers and they can easily construct complex logical systems & statements to be solved and analyzed by LFI in very short time and great efficiancy , Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
                    </p>
                </div>
                <div class="col-sm-4">
                    <h3>Easy to use</h3>
                    <p>
                        LFI guarantees that complex logical statements are solved correctly and processed in short time and simply viewed to the user in a freindly way with great readablity and adaptability with analysis with examples related to the input that the user entered , Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident. 

                    </p>
                </div>
                <div class="col-sm-4">
                    <h3>Always there's a new</h3>
                    <p>
                        LFI is looking for an evolution to be updated and marketed with a strategy to target logical students all around the world and in major colleges or all colleges alike and there will be a community for logical interests that starts within Egypt  . Also LFI is looking to be on other platforms such as mobile platforms and the team is already starting to develop an android application to publish , LFI is free for all !
                    </p>
                </div>

            </div>
        </div>
    </section>
    <section id="team">
        <div class="container">
            <h2>Meet our team</h2>
            <div class="row">
                <div class="col-sm-3">
                    <img src="/img/Team/Basil.jpg" class="img-circle" />
                    <h4>Basil Aiman</h4>
                    <p>
                        Full Stack Web Developer
                    </p>
                </div>
                <div class="col-sm-3">
                    <img style="height: 185px;" src="/img/Team/Khalid3.jpg" class="img-circle" />
                    <h4>Khalid Sulaiman</h4>
                    <p>
                        Back-End Developer
                    </p>
                </div>
                <div class="col-sm-3">
                    <img style="width: 200px; height: 185px;" src="/img/Team/Walid1.jpg" class="img-circle" />
                    <h4>Ahmed Walid</h4>
                    <p>
                        Web Designer  
                    </p>
                </div>
                <div class="col-sm-3">
                    <img src="/img/Team/Ashraf.jpg" class="img-circle" />
                    <h4>Ashraf El Blacy</h4>
                    <p>
                        System Analyzer 
                    </p>
                </div>
            </div>
        </div>
    </section>
    <section id="signup">
        <div class="container">
            <h2>Subscribe for more </h2>
            <p class="lead">Seriously ,We Will send you some stuff</p>

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="input-group">
                        <input type="text" class="form-control input-lg" id="txtEmail" runat="server" placeholder="Put your Email" />
                        <span class="input-group-btn">
                            <button class="btn btn-warning btn-lg" type="button" id="btnSubscribe" runat="server" onserverclick="btnSubscribe_ServerClick" validationgroup="e">Save it !</button>

                        </span>
                        
                    </div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ForeColor="Red" ErrorMessage="Enter E-mail correctly" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="e" ControlToValidate="txtEmail" Display="Dynamic"></asp:RegularExpressionValidator>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
