﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LFS
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO : Email Validation in Subscribe Section 

            if (Request.IsAuthenticated)
            {
                btnLog.Visible = false;
                btnSolve.Visible = true;
            }
        }

        protected void btnSubscribe_ServerClick(object sender, EventArgs e)
        {
            if (IsValid)
            {
                LFSEntities db = new LFSEntities();
                db.tblEmails.Add(new tblEmails { Email = txtEmail.Value });
                db.SaveChanges();
            }
            

        }
    }
}